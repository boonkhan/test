import { valueToNode } from "@babel/types"

export const something=[
    {
        name:'wanisa',
        city:'united state',
        skill:[{
            Language:'java'

        },
        {
            Language:'php'
        }]
    },
    {
        name:'yanisa',
        city:'singapore',
        skill:[{
            Language:'java'

        },
        {
            Language:'c++'
        }]
    },
]

export const what=[
    {
        title:'password',
        ref:'wanisa',
        xs:'4',
        ls:'5',
        sm:'6',
        
      
    }
]
export const news=[
    {
        id:'1',
        img:require('../../Asset/img/kkk.JPG'),
        title:`Climate change: British Airways reviews 'fuel-tankering' over climate concerns`,
        detail:'It follows a BBC investigation exposing "fuel tankering" by airlines - in which planes are filled with extra fuel, usually to avoid paying higher prices for refuelling at destination airports.'
        
      
    },
    {
        id:'2',
       img:require('../../Asset/img/kkk.JPG'),
        title:'Climate change: Bigger hurricanes are now more damaging',
        detail: `Using a new method of calculating the destruction, the scientists say the increase in frequency is "unequivocal"
                 Previous attempts to isolate the impact of climate change on hurricanes have often came up with conflicting results.`
        
      
    },
    {
        id:'3',
        img:require('../../Asset/img/kkk.JPG'),
        title:'Bird of the Year: Rare anti-social penguin wins New Zealand poll',
        detail:`The hoiho saw off more than five rivals to become the first penguin to win the annual honour in its 14-year history.

        With only 225 pairs left on New Zealand's mainland, the hoiho is said to be the rarest penguin in the world.'`
        
      
    },
    {
        id:'4',
        img:require('../../Asset/img/kkk.JPG'),
        title:`Hillary Clinton: 'Shameful' not to publish Russia report`,
        detail:`It is "inexplicable and shameful" that the UK government has not yet published a report on alleged Russian interference in British politics, Hillary Clinton has told the BBC.

        The report has formal security clearance, but it will not be released until after the 12 December election.`
        
      
    }

]