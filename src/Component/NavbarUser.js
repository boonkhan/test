import React, { Component } from 'react'
import { Link } from 'react-router-dom';

export default class NavbarUser extends Component {
    logout=()=>{
        window.localStorage.removeItem('role')
        window.location.reload()
    }
    render() {
        let path=window.location.pathname
        return (
            <div >
                 <Link className="active" to={'/page1'}>page1</Link>
                <Link  className="active" to={'/page2'}>page2</Link>
                <Link  className="active" to={'/page3'}>page3</Link>
                <button onClick={this.logout}>log out</button>
            </div>
        )
    }
}
