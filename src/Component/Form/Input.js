import React from 'react'
import {Row,Col} from 'reactstrap'

export function InputFormGroup({data,onchangeText}){
    let{title,ref,xs,sm,md,lg,ls,value}=data
    return (
       <Row>
           <Col xs={xs} sm={sm} ls={ls}>
           <div className="form-group">
    <label>{title}</label>
               <input className="form-control"
               name={ref}
              onChange={onchangeText}
              value={value}/>
               </div>
           </Col>
       </Row>
        
     
    )
}