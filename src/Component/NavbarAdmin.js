import React, { Component } from 'react'
import { Link } from 'react-router-dom';

export default class NavbarAdmin extends Component {
    logout=()=>{
        window.localStorage.removeItem('role')
        window.location.reload()
    }
    render() {
        let path=window.location.pathname
        return (
            <div >
       
                <Link  className="active" to={'/admin1'}>admin1</Link>
                <Link  className="active" to={'/admin3'}>admin2</Link>
                <button className="btn-success" onClick={this.logout}>log out</button>
            </div>
        )
    }
}
