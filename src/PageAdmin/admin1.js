import React, { Component } from 'react'
import{news} from'../Component/MobData/data'
import { InputFormGroup } from '../Component/Form/Input'
// import { element } from '../../../../../AppData/Local/Microsoft/TypeScript/3.6/node_modules/@types/prop-types'


export default class admin1 extends Component {
    constructor(props){
        super(props)
        this.state={
        nn:[],
        oldnn:[]
        }
    }
    componentDidMount(){
        this.fetchItem()

    }
    async fetchItem(){
        try {
            const items=await news
            this.setState({nn:items,oldnn:items})
            console.log('item',items)
        } catch (error) {
            console.log('error',error)
            
        }
        console.log('error')
       
    }
    onChangeText=(e)=>
    {
        this.setState({[e.target.name] :e.target.value})
    }

    onSearch=(e)=>{
        console.log('something',e.target.value)
        let {oldnn}=this.state
        let value=e.target.value
        
        if (value!=='') {
            let solution=oldnn.filter(el=>{
            return(
                el.title.includes(value)
            )
            })
            this.setState({nn:solution})
            

            
        }else{
            this.setState(prev=>({nn:prev.oldnn})  )
        }
    
        


    }
    onClicknews=(id)=>{
        
       this.props.history.push({
           pathname:'/admin2',
           state :{id}
       })
    
    
     
        
      }
    render() {
        let {nn}=this.state
        return (
            <div>
                <div className="mt-3">
                <InputFormGroup data={{title :'ค้นหา',md :6}} onchangeText={this.onSearch} />
                </div>
            <div className="mt-5">
               {nn.map((e,i)=>
               <div className="d-flex  align-Items-center">
                   <h4>
                  {i+1}. {e.title}
                   {e.detail} </h4>
                   <span className="text-info-pb-1" style={{cursor:'pointer'}}
                    onClick={()=>this.onClicknews(e.id)}> &nbsp; อ่านต่อ </span>
                 
                  
                   {/* skill:{element.skill.map((e)
                   => <p>
                       
                        </p>
                        } */}

                   </div>
              ) }
              
            </div>
            </div>
        )
    }
}
