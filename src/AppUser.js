import React, { Component } from 'react'
import { withRouter,Link } from 'react-router-dom';
import  NavbarUser  from './Component/NavbarUser';

class AppUser extends Component {
    constructor(props){
        super(props)
        this.state={

        }
    }
    render() {
        return (
            <React.Fragment>
         
                <header className="user-navbar">
                <div className="set-user-navbar">
                    <NavbarUser/>
               </div>
                </header>
               
                <div className="set-page"> 
                {this.props.children}
                </div>
            <footer className="user-footer"></footer>
            </React.Fragment>
        )
    }
}
export default withRouter(AppUser)
