import React, { lazy, Suspense } from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

const UserRoute = lazy(() => import('./UserRoute'))
const Login = lazy(() => import('../Page/Login'))
const AdminRoute = lazy(()=> import('./AdminRoute') )
const user = {
    role: window.localStorage.getItem('role')
}
export default () =>
    <BrowserRouter>
        <Suspense fallback={false}>
            <Switch>
                <Route path="/login"  component={Login} />
                {user && user.role=== 'user' ? 
                    <UserRoute />
                    :user && user.role=== 'admin' ? 
                    <AdminRoute />

                    : <Login />}
                    
            </Switch>
        </Suspense>
    </BrowserRouter>
