import React, { lazy} from 'react'
import { Route,Switch } from 'react-router-dom'

const AppUser = lazy(() =>import('../AppUser'))
const Page1=lazy(() =>import('../Page/Page1'))
const Page2=lazy(() =>import('../Page/Page2'))
const Page3=lazy(() =>import('../Page/Page3'))

export default() =>
<AppUser>
    <Switch>
        <Route path="/" exact component={Page1}/>
        <Route path="/Page1" exact component={Page1}/>
        <Route path="/Page2" exact component={Page2}/> 
        <Route path="/Page3" exact component={Page3}/>
        
        </Switch>
        </AppUser>

