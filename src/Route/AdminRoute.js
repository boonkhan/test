import React, { lazy} from 'react'
import { Route,Switch } from 'react-router-dom'
import Login from '../Page/Login'

const AppUser = lazy(() =>import('../AppAdmin'))
const Page1=lazy(() =>import('../Page/Page1'))
const admin1=lazy(() =>import('../PageAdmin/admin1'))
const admin2=lazy(() =>import('../PageAdmin/admin2'))
const admin3=lazy(() =>import('../PageAdmin/admin3'))


export default() =>
<AppUser>
    <Switch>
        <Route path="/" exact component={admin1}/>
        <Route path="/admin1" exact component={admin1}/>
        <Route path="/admin2" exact component={admin2}/> 
        <Route path="/admin3" exact component={admin3}/> 
        
        </Switch>
        </AppUser>